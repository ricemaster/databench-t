/*
 * Copyright 2022-2027 中国信息通信研究院云计算与大数据研究所
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ 
package com.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mapper.ReadDataConfig;
import com.mapper.ReadTranConfig;
import com.pojo.DataConfig;
import com.pojo.Trancfg;
import com.service.ConfigureService;

@Service
public class ConfigureServiceImpl implements ConfigureService {

    @Autowired
    private ReadTranConfig readTranConfig;

    @Autowired
    private ReadDataConfig readDataCfg;
    
    @Override
    public DataConfig readDataConfig(String datacnfg_id) {
        DataConfig df = readDataCfg.readDataCfg(datacnfg_id);
      	return df;
    }

    @Override
    public Trancfg readTranConfig(String trancfg_testid,String accountType) {
        Trancfg param = new Trancfg();
        param.setTrancfg_testid(trancfg_testid);
        param.setTrancfg_id(accountType);
      	List<Trancfg> tfList = readTranConfig.readTranConfig(param);
      	if(tfList.size()>0) {
      		return tfList.get(0);
      	}else {
      		return null;
      	}
    }
}